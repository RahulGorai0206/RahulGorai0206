
## 🚀 Who I Am ?
Just Another Github Guy .....
Nickname **Wisky**


## 😎 Appendix
👩‍💻 I'm currently learning working on *AOSP*

🧠 I'm currently learning Java, Full Stack Web Development

💬 Languages :- Bengali, Hindi, English

📫 Currently at West Bengal , India 

⚡️ Fun fact :- Me Noob


## 🛠 Skills

**Current:** Java, HTML, CSS, C, Android, Windows, Linux


## 😇 Lessons Learned

- Spread love, happiness.
- Anything that you think is't always real or ture.
- Everytime make a smile.
- Think twice and always try to take a look to others side also before taking any action.
- **Thats it, if i remember :D**


## 🫂 Support

For query, email rahulgorai0206@gmail.com or join our Telegram Group.


## 🔗 Links


[![Telegram I'd](https://img.shields.io/badge/Telegram%20-ID-blue)](https://telegram.me/RahulGorai)

[![Telegram Channel](https://img.shields.io/badge/Telegram%20-channel-green)](https://telegram.me/wisky_rum)

[![Telegram Group](https://img.shields.io/badge/Telegram%20-Group-blue)](https://telegram.me/wisky_chat)



## ☕️ Buy Me A Coffee
[![Buy Me A Coffee](https://img.shields.io/badge/Buy%20Me-A%20%20Coffee-9cf)](https://ko-fi.com/rahulgorai)

## 👀 Visitors

![visitors](https://visitor-badge.laobi.icu/badge?page_id=rahulgorai0206.rahulgorai0206)
